# Using Lando to set up your local development environment

## TOC

<!-- toc -->

- [Wordpress using Bedrock from scratch](#Wordpress-using-Bedrock-from-scratch)
  * [Requirements](#Requirements)
  * [1. Create a new project directory](#1-Create-a-new-project-directory)
  * [2. Initialize lando](#2-Initialize-lando)
  * [3. Use lando's built in composer command and install Bedrock inside a new `app` directory](#3-Use-landos-built-in-composer-command-and-install-Bedrock-inside-a-new-app-directory)
  * [4. Edit Bedrock's .env file inside the `app` directory](#4-Edit-Bedrocks-env-file-inside-the-app-directory)
  * [5. Start lando](#5-Start-lando)
  * [6. Access your app in a browser and proceed with the Wordpress installation](#6-Access-your-app-in-a-browser-and-proceed-with-the-Wordpress-installation)
- [Simple Wordpress setup](#Simple-Wordpress-setup)
- [Migrating an existing Wordpress project to lando](#Migrating-an-existing-Wordpress-project-to-lando)
  * [Wordpress using Bedrock](#Wordpress-using-Bedrock)
- [Other project environments](#Other-project-environments)
- [Accessing your app via htpps](#Accessing-your-app-via-htpps)
  * [Importing Lando's CA in Firefox](#Importing-Landos-CA-in-Firefox)
  * [Importing Lando's CA in Chrome](#Importing-Landos-CA-in-Chrome)
- [Additional project setup procedures](#Additional-project-setup-procedures)
- [Other Useful Features](#Other-Useful-Features)
- [General Notes](#General-Notes)
- [Issues and Fixes](#Issues-and-Fixes)
  * [WP + Bedrock Setup](#WP-Bedrock-Setup)
- [Additional Guides and References](#Additional-Guides-and-References)

<!-- tocstop -->

## Wordpress using Bedrock from scratch

**Important note:**

There's an existing issue on Github where Lando conflicts with Bedrock's use of a .env file. You can read details of the issue here: [WordPress: Doesn't work with Bedrock framework · Issue #499](https://github.com/lando/lando/issues/499). There are a couple of workarounds proposed including removing DB variable entries and Wordpress hash keys in the .env file, none of which worked for me on my tests (see issues encountered below). There's even a fork made of lando specifically for addressing these bedrock issues but installing a forked version for general local development setup is a far from ideal solution.

The only working solution I found so far, without resorting to installing a forked version of lando, is the one suggested last in the Github thread,  where you put the whole bedrock project in it's own directory inside a lando project folder. This setup is the one outlined below.

### Requirements
1. Lando: [Install using instructions for your OS here.](https://docs.devwithlando.io/installation/installing.html)
3. Docker Community Edition

### 1. Create a new project directory

```
$ mkdir <appname>
$ cd <appname> 
```    

### 2. Initialize lando

```
$ lando init
```

- Choose wordpress recipe.
- Just leave Pantheon machine token blank (hit enter).
- Define your webroot as `app/web`. This directory will be created later.
- Define a short descriptive name for the project (e.g., Test Project). lando will create a simplified name (e.g., test-project) based on what you entered. 

![03347afd.png](attachments/c7b39079.png)

This should create a `.lando.yml` file inside the project directory with the following content.

![46a145ce.png](attachments/7dee61a1.png)

### 3. Use lando's built in composer command and install Bedrock inside a new `app` directory

```
$ lando composer create-project roots/bedrock app
```

The above commands installs bedrock in an `app` folder inside the newly-created project directory. The main project directory is where you run lando commands to manage the app without lando conflicting or interfering with bedrock's .env settings. 

If you already have composer installed on your system then you can just run

```
$ composer create-project roots/bedrock app
```

![82bb98f8.png](attachments/ba434b43.png)

### 4. Edit Bedrock's .env file inside the `app` directory

1. Set DB_NAME, DB_USER, and DB_PASSWORD to 'wordpress' (default DB creds set by lando, you can see this and other lando app configuration settings by running `lando info`).
2. Uncomment (if commented out) DB_HOST and set value to 'database'.
3. Change WP_HOME variable to local app url (e.g., http://appname.lndo.site)
4. Generate and copy paste over keys from here: [WordPress Salts](https://roots.io/salts.html)

![Selection_493.png](attachments/27121b00.png)

### 5. Start lando

```
$ lando start
```

![a2402c28.png](attachments/f416b338.png)

If the app's URLs are showing up in red that means that lando cannot detect a wordpress installation running in the defined web root (app/web), you may need to run `lando rebuild` to fix this.

### 6. Access your app in a browser and proceed with the Wordpress installation

![53ed5f3c.png](attachments/9df5fa76.png)

## Simple Wordpress setup

Guide: [WordPress · Lando Documentation](https://docs.devwithlando.io/tutorials/wordpress.html)

## Migrating an existing Wordpress project to lando

### Wordpress using Bedrock

1. Change the project's local directory structure to prevent issue with bedrock's use of .env file.
    - Go to the project directory and create an `app` folder.
    - Transfer all existing files and folders in the project directory to the newly created `app` folder, taking care that you transfer everything including hidden files (i.e., .env) and directories (i.e., .git folder).    

2. Initialize lando by running `$ lando init` inside the project folder.
    - Choose `wordpress` recipe.
    - Just leave Pantheon machine token blank (hit enter).
    - Define your webroot as `app/web`.
    - Enter a short descriptive name for the project (e.g., Test Project). lando will create a simplified name (e.g., test-project) based on what you entered. 
    - After successfully running the command the project folder should only contain the `app` folder and the newly created lando config file (.lando.yml).

3. Run `$ lando start`.
4. Make changes to bedrock's `.env` file.
    - Set DB_NAME, DB_USER, and DB_PASSWORD to 'wordpress' (default DB creds set by lando, you can see this and other lando app configuration settings by running `lando info`).
    - Set DB_HOST value to 'database'.
    - Change WP_HOME variable to local app url (e.g., http://appname.lndo.site)
5. Access the app using your browser and continue with Wordpress installation or [perform other project setup procedures](#Additional-project-setup-procedures) as may be needed.

## Other project environments

There are readily available lando recipes for the following development environments:

- Backdrop
- Drupal
- Joomla
- Laravel
- LAMP
- LEMP
- MEAN
- Pantheon

You can see instructions for these in the Recipes section of the [official lando docs](https://docs.devwithlando.io/).

If there's no available recipe for your project you can just build up an environment from a base language. Instructions are available in the docs for the following languages:

- [dotnet](http://docs.devwithlando.io/tutorials/dotnet.html)
- [go](http://docs.devwithlando.io/tutorials/go.html)
- [node](http://docs.devwithlando.io/tutorials/node.html)
- [php](http://docs.devwithlando.io/tutorials/php.html)
- [python](http://docs.devwithlando.io/tutorials/python.html)
- [ruby](http://docs.devwithlando.io/tutorials/ruby.html)

## Accessing your app via htpps

Lando automatically provides an option to access the app via https, as well as the normal http protocol, without the need to manually create self-signed certificates or install a tool like mkcert for local development. 

![f4859f3e.png](attachments/72d172f0.png)

The problem is that you may still get browser warnings when trying to access the app using its https URL. 

![1e144d09.png](attachments/8bb3cc9c.png)

Lando uses its own CA and by default this can be located at ```~/.lando/certs/lndo.site.pem```. There are instructions for installing the certificate on your system in the [lando docs](https://docs.devwithlando.io/config/security.html) so that the CA would be trusted locally, but I found that this doesn't seem to take effect and I still had browser warnings when acessing the app via https. 

After some research I found that browsers such as Firefox and Chrome make use of their own certificate stores independent from that of the local system's. And you have to add Lando's CA into each browser to remove these warnings.

### Importing Lando's CA in Firefox

1. In Preferences > Privacy & Security, Certificates section, click 'View Certificates' 
2. In Authorities tab, click on Import
3. Import the default Lando CA located at ```~/.lando/certs/lndo.site.pem```, check 'Trust this CA to identify websites', and click OK 

![93cfa5a2.png](attachments/d3a98fed.png)

### Importing Lando's CA in Chrome

1. In Settings, search for 'certificate', and click on 'Manage certificates'
2. In Authorities tab, click Import
3. Import the default Lando CA located at ```~/.lando/certs/lndo.site.pem```, check 'Trust this certificate for identifying websites', and click OK

![f79b3636.png](attachments/c3c0e582.png)

Open the app in your browser using the app's https URL and the privacy warning should be gone.

**This proceess only needs to be done once for browsers to trust all apps using the lndo.site local domain.**

## Additional project setup procedures

- [Using NodeJS Frontend Tooling (yarn, gulp, etc.)](https://docs.devwithlando.io/guides/frontend.html)

- [Setting Up PhpUnit Debugging in Visual Studio Code](https://docs.devwithlando.io/guides/lando-with-vscode.html)

- [TODO] Installing and working with Sage starter theme

## Other Useful Features

- [Exporting](http://docs.devwithlando.io/guides/db-export.html) and [Importing](http://docs.devwithlando.io/guides/db-import.html) Databases

- [Making your local site accessible to the public](https://docs.devwithlando.io/cli/share.html)

- [Accessing Lando from other devices on your local network (e.g., phone, tablet)](https://docs.devwithlando.io/guides/access-by-other-devices.html)


## General Notes

1. Lando exposes commands like `composer`, `php` and `wp` (for WP-CLI commands) which you can use without needing to install these tools locally on your machine. To see all available commands for lando just run `lando` without any options or arguments.
2. Need to run ```lando rebuild``` every time you make manual changes to the .lando.yml file.
3. Contrary to documentation, seems like running ```lando destroy``` inside an app's directory does not completely destroy that app and you can still see the app listed when running ```lando list```. Need to run ```lando destroy <appname>``` outside the app's directory for that. 

## Issues and Fixes

### WP + Bedrock Setup

**Error on `lando start`: Invalid interpolation format for "environment" option in service "appserver"**

![0f2c24a5.png](attachments/b1db7a3d.png)

This issue is mentioned here: [WordPress: Doesn't work with Bedrock framework · Issue #499 · lando/lando · GitHub](https://github.com/lando/lando/issues/499)

Lando seems to conflict with DB environment variables and gets confused with the SECURE_AUTH_KEY in the .env file when running ```lando start```. The proposed workaround is to comment out all the DB variables and Wordpress hash key values in bedrock's .env file and run ```lando start```, then uncomment the hash key values when lando is already running. 

When I did this I encountered another error when accessing the local app through a browser.

![3249ce9d.png](attachments/dac7afd1.png)

Seems like bedrock is now looking for the DB variables removed from the .env file. 

There's a fork for lando that ignores .env for bedrock setup here: [Tandem's Fork of Bedrock](https://github.com/thinktandem/bedrock), but these solutions are cumbersome for local dev (you have to do the .env workaround every time you start lando) and not ideal for the general usage of lando in setting up other development environments (ones not needing wordpress and bedrock, for example). 

**The only working solution for now is to isolate bedrock in it's own directory as outlined above.**

## Additional Guides and References
1. [Lando Documentation](https://docs.devwithlando.io/)
2. [An Intro to Bedrock for WordPress \| CSS-Tricks](https://css-tricks.com/intro-bedrock-wordpress/)
2. [GitHub - lando/lando: You look absolutely beautiful!](https://github.com/lando/lando)